import React, { Component } from 'react';
import './App.css';
import './components/macros/macros';

//components
import BasicFormular from './components/basicFormular';
import ValidateFormular from './components/validationFormular';
import NestedFormular from'./components/nestedFormular';
import ConnectedFormular from './components/childComponents/parentComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
          <h1>Basic formular</h1>
          <BasicFormular/>
          <h1>Validation formular</h1>
          <ValidateFormular />
          <h1>Nested formular</h1>
          <NestedFormular />
          <h1>Connected Child Formular</h1>
          <ConnectedFormular />
      </div>
    );
  }
}

export default App;
