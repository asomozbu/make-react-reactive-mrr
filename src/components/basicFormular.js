import React from 'react';
import { withMrr } from 'mrr';

const maxLength = (max) => {
    return (value) => {
        return value.length <= max ;
    }
};

const minLength = (min) => {
    return (value) => {
        return value.length >= min ;
    }
};

export default withMrr({
    $init: {
        a: '',
        b: '',
    },
    c: [(n, m) => {
        const num = Number(n) + Number(m);
        return isNaN(num) ? '' : num;
    }, 'a',  'b'],
    d: ['validate', [maxLength(6), minLength(2)], 'd'],
}, (state, props, $) => (
    <div>
        <input value={ state.a } onChange={ $('a') } />
        +
        <input value={ state.b } onChange={ $('b') } />
        =
        <input value={ state.c } disabled/>
        <input value={ state.d } onChange={$('d')} />
        {state['d.error']}
    </div>
));