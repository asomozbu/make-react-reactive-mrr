import React from 'react';
import { withMrr } from 'mrr';

const errorMessage = (valid) => (valid ? 'ahora si baby!' : 'error de formulario');
const allTrue = (a,b) => a && b;

export default withMrr({
    $init: {
        name: '',
        email: '',
        message: '',
    },
    nameValid: [a => a.length > 2, 'name'],
    emailValid: [a => a.match(/.*\@.*/), 'email'],
    formValid: [allTrue, 'nameValid', 'emailValid'],
    message: [errorMessage, 'formValid'],
    submitForm: [(valid, name, email) => {
        if (valid) {
            // make some AJAX call with name and email…
        }
    }, 'formValid', 'name', 'email', 'submit']
}, (state, props, $) => ( <form>
    <p>{state.message}</p>
    <p>{props.name}</p>
    Enter your name:
    <input value={ state.name } onChange={ $('name') }/>
    <span>{state.nameValid? 'ok' : ''}</span><br/>
    Enter your email:
    <input value={ state.email } onChange={ $('email') }/>
    <span>{state.emailValid? 'ok' : ''}</span><br/>
    <input type="submit" onClick={ $('submit') }/>
</form> ));