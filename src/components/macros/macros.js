import { registerMacros } from 'mrr';

const reduceValidationToMark = value => (acc, currValidator) => acc + (currValidator(value) ? 1 : 0);

registerMacros('validate', ([func, ...args]) => ['nested', function(cb, ...values) {
    const value = values[0];
    const testMark = func.reduce(reduceValidationToMark(value), 0);

    if(testMark === func.length) {
        cb('value', values[0]);
        cb('error', false);
    } else {
        cb('value', null);
        cb('error', 'errorrrrrrrr');
    }
}, ...args]);
