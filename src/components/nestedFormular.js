import React from 'react';
import { withMrr } from 'mrr';
import * as R from 'ramda';

const returnElectronicData = (cb, value, electronicList) => {
    const valueList = R.is(Array, electronicList) ? [...electronicList, value] : [value];
    cb('loading', true);
    setTimeout(() => {
        cb('loading', false);
        cb('list', valueList);
    }, 1000);
};

export default withMrr({
    $init: {
        goods: [],
        sortByField: 'price',
        list: [],
    },
    selectedCategory: [(e) => e,'selectCategory'],
    sortByField: [a => a.toLowerCase(), 'sortBy'],
    electronicList: ['nested', returnElectronicData, 'selectedCategory', '-electronicList.list'],
    list: [(e) => e, 'electronicList.list'],
}, (state, props, $) => (
    <div>
        <select value={ state.selectedCategory } onChange={ $('selectCategory') }>
            <option>Cars</option>
            <option>Electronics</option>
            <option>Audio</option>
        </select>
        <select value={ state.sortByField } onChange={ $('sortBy') }>
            <option>Name</option>
            <option>Price</option>
        </select>
        {
            state['electronicList.loading']
                ? 	<span>
                        'Loading...'
                        <ul>
                        { state.list.map(g => <div>{ g }</div>) }
                        </ul>
                    </span>
                :   <ul>
                        { state.list.map(g => <div>{ g }</div>) }
                    </ul>
        }
    </div>
));