import React from 'react';
import { withMrr } from 'mrr';

import FirstChild from './firstChildComponent';
import SecondChild from './secondChildComponent';

export default withMrr({
    $init: {
        todos: [],
        colorWinner: 0,
    },
    todos: [(arr, new_item) => {
        debugger;
        arr.push(new_item);
        return arr;
    }, '^', 'add_todo/new_todo'],
    colorWinner: ['funnel', (cellName, cellValue) => {
        if(cellValue > 5) {
            switch(cellName) {
                case 'second_child/blueCounter':
                    return 1;
                case 'second_child/redCounter':
                    return 2;
                case 'second_child/yellowCounter':
                    return 3;
                default:
                    return 0;
            }
        }
    }, 'second_child/blueCounter', 'second_child/redCounter', 'second_child/yellowCounter'],
}, (state, props, $, connectAs) => (
    <div>
        <ul>
            { state.todos.map(todo => <li>{ todo }</li>) }
        </ul>
        <FirstChild {...connectAs('add_todo')} />
        {state.colorWinner && state.colorWinner + 'wins'}
        <SecondChild {...connectAs('second_child')} />
    </div>
));