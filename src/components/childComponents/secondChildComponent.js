import React from 'react';
import { withMrr } from 'mrr';

const counter = () => {
    let count = 0;
    return () => ++count;
};

export default withMrr({
    // returns new todo object when submit is pressed
    blueCounter: ['closure', () => counter(), 'blueCount'],
    redCounter: ['closure', () => counter(), 'redCount'],
    yellowCounter: ['closure', () => counter(), 'yellowCount'],
    test: ['promise', (e) => e, 'redCount'],
}, (state, props, $) => (
    <div>
        <button onClick={$('redCount')} style={{backgroundColor: 'red'}}>Red</button>
        <button onClick={$('blueCount')} style={{backgroundColor: 'blue'}}>Blue</button>
        <button onClick={$('yellowCount')} style={{backgroundColor: 'yellow'}}>Yellow</button>
    </div>
));