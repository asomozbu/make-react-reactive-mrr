import React from 'react';
import { withMrr } from 'mrr';

export default withMrr({
    // returns new todo object when submit is pressed
    new_todo: [(text, submit) => (text), '-text', 'submit'],
}, (state, props, $) => (
    <form>
        <input type="text" onChange={ $('text') } />
        <input type="submit" onClick={ $('submit') } />
    </form>
));