import React from 'react';
import { withMrr } from 'mrr';

export default withMrr({

}, (state, props, $) => (
    <div>
        {props.children}
    </div>
));